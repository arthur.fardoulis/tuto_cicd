FROM	maven:latest
COPY	./pom.xml .
COPY	./src/main/java/hangman/*.java src/main/java/hangman/
RUN	mvn clean package
CMD	java -jar target/Hangman-jar-with-dependencies.jar
