package hangman;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ModelTest {

    @Test
	public void testModel() {
		Model model = new Model("Chocolate");
		assertEquals("CHOCOLATE", model.objective);
		assertEquals("---------", model.current);
		assertEquals("", model.errors);
	}

    @Test
	public void testTryLetter() {
		Model model = new Model("Chocolate");
		boolean b = model.tryLetter('o');
		assertTrue(b);
		assertEquals("--O-O----", model.current);
		assertEquals("", model.errors);
		b = model.tryLetter('c');
		assertTrue(b);
		assertEquals("C-OCO----", model.current);
		assertEquals("", model.errors);
		b = model.tryLetter('x');
		assertFalse(b);
		assertEquals("C-OCO----", model.current);
		assertEquals("X", model.errors);
	}

}
